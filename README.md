# Cpp_Calc
Este proyecto es un ejemplo básico del uso de ```Bison``` y ```Flex``` utilizando ```c++``` basado en el ejemplo provisto por [GNU](https://www.gnu.org/software/bison/manual/html_node/A-Complete-C_002b_002b-Example.html)

## Dependencias
- ```GCC```
- ```Bison```
- ```Flex```
- ```Make```
- ```CMake```

## Compilación
```bash
    $ mkdir build 
    $ cd build
    $ cmake ..
    $ make
```
Opcionalmente puede utilizar el siguiente comando para instalar cpp_calc en el sistema:
```bash
    $ sudo make install
```

## Ejecución
Despues de terminar la compilación, el ejecutable va a estar en la carpeta build y hay un ejemplo de un archivo aceptado por la calculadora en la carpeta ```test``` que puede usar para probar el funcionamiento de la misma.
Para probar la calculadora, use el siguiente comando:
```bash
    ./calc ../test/in.txt
```